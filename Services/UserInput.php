<?php

namespace Services;

use TextOperation\TextOperationInterface;

class UserInput
{
    const OPERATIONS = ['Cut HTML tags', 'Wrap text in tags <p> <br/>'];
    const METHODS = ['cutTags', 'wrapText'];

    private $textOperation;

    /**
     * UserInput constructor.
     * @param TextOperationInterface $textOperation
     */
    public function __construct(TextOperationInterface $textOperation)
    {
        $this->textOperation = $textOperation;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function definitionInput()
    {
        echo "Formatting text\nPlease enter number\n";
        for ($i = 0; $i < count(self::OPERATIONS); $i++) {
            echo ($i + 1).' - '.self::OPERATIONS[$i]."\n";
        }
        $numberCommand = fgets(STDIN);

        echo "1 - Console \n2 - File\n";

        $dataType = fgets(STDIN);

        if ($dataType == 1) {
            echo "Enter text\n";
        } else if ($dataType == 2){
            echo "Enter file path\n";
        } else {
            throw new \Exception('Error input!!');
        }
        $data = fgets(STDIN);

        return ['numberCommand' => $numberCommand, 'dataType' => $dataType, 'data' => $data];
    }

    /**
     * @param array $fields
     * @return mixed
     * @throws \Exception
     */
    public function formatText(array $fields)
    {
        $command = (int)$fields['dataType'];
        if ($command > count(self::OPERATIONS)) {
            throw new \Exception('Operation not found');
        }
        $method = self::METHODS[(int)$fields['numberCommand'] - 1];
        switch ($command) {
            case 1:
                $resultMethod = $this->textOperation->$method(trim($fields['data']));
                echo 'Result:';
                echo $resultMethod;
                echo "\n";
                break;
            case 2:
                $file = trim($fields['data']);
                $readFile = file_get_contents($file, true);
                if ($readFile === false) {
                    throw new \Exception('File not found!');
                }
                $resultMethod = $this->textOperation->$method($readFile);

                file_put_contents($file, $resultMethod);

                return $this->textOperation->$method($file);
        }
    }
}