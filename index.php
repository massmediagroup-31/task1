<?php

use Services\UserInput;
use TextOperation\TextOperation;

require 'vendor/autoload.php';

$textOperation = new TextOperation();
$userInput = new UserInput($textOperation);
$data = $userInput->definitionInput();
$formattedText = $userInput->formatText($data);

echo "Ok\n";

