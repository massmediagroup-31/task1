<?php

namespace TextOperation;

/**
 * Class for work with text operations
 * Class TextOperation
 * @package TextOperation
 */
class TextOperation implements TextOperationInterface
{
    /**
     * Delete html tags
     * @param $text
     * @return string
     */
    public function cutTags(string $text)
    {
        return strip_tags($text);
    }

    /**
     * Wrap text in <p></p>
     * @param $text
     * @return string
     */
    public function wrapText(string $text)
    {
        $textArray = explode("\n", $text);
        $formattedArray = [];

        foreach ($textArray as $key => $value) {
            $formattedArray[] = '<p>' . trim($value) . '</p>';
        }
        $formattedText = implode("\n",$formattedArray);

        return $formattedText;
    }
}