<?php

namespace TextOperation;

/**
 * Interface TextOperationInterface
 * @package TextOperation
 */
interface TextOperationInterface
{
    public function cutTags(string $text);
    public function wrapText(string $text);
}